---
layout: league-comparison
code: "pt"
competition: "Primeira Liga"
country: "Portugal"
api: true
mostPoints: 88
fewestPoints: 0
seasons:
  - season: "2016/17"
    active: true
  - season: "2017/18"
    active: true
  - season: "2018/19"
    active: true
  - season: "2019/20"
    active: true
  - season: "2020/21"
    active: true
  - season: "2021/22"
    active: true
---
