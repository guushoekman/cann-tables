---
layout: league-comparison
code: "tr"
competition: "Süper Lig"
country: "Turkey"
mostPoints: 75
fewestPoints: 12
seasons:
  - season: "2017/18"
    active: true
  - season: "2018/19"
    active: true
---
