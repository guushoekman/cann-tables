---
layout: league-comparison
code: "ru"
competition: "Russian Premier League"
country: "Russia"
mostPoints: 64
fewestPoints: 20
seasons:
  - season: "2018/19"
    active: true
---
