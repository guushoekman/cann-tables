---
layout: league-comparison
code: "md"
competition: "Divizia Națională"
country: "Moldova"
mostPoints: 63
fewestPoints: 14
seasons:
  - season: "2017/18"
    active: true
  - season: "2018/19"
    active: true
---
