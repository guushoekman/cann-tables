---
layout: league-comparison
code: "nl"
competition: "Eredivisie"
country: "Netherlands"
api: false
mostPoints: 88
fewestPoints: 15
seasons:
  - season: "2017/18"
    active: true
  - season: "2018/19"
    active: true
  - season: "2019/20"
    active: true
  - season: "2020/21"
    active: true
  - season: "2021/22"
    active: true
---
