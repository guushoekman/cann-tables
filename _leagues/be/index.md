---
layout: league-comparison
code: "be"
competition: "Eerste Klasse A"
country: "Belgium"
mostPoints: 70
fewestPoints: 20
seasons:
  - season: "2018/19"
    active: true
  - season: "2019/20"
    active: true
---
