---
layout: league-comparison
code: "es"
competition: "La Liga"
country: "Spain"
api: true
mostPoints: 100
fewestPoints: 0
seasons:
  - season: "1999/00"
  - season: "2000/01"
  - season: "2001/02"
  - season: "2002/03"
  - season: "2003/04"
  - season: "2004/05"
  - season: "2005/06"
  - season: "2006/07"
  - season: "2007/08"
  - season: "2008/09"
  - season: "2009/10"
  - season: "2010/11"
  - season: "2011/12"
  - season: "2012/13"
  - season: "2013/14"
  - season: "2014/15"
    active: true
  - season: "2015/16"
    active: true
  - season: "2016/17"
    active: true
  - season: "2017/18"
    active: true
  - season: "2018/19"
    active: true
  - season: "2019/20"
    active: true
  - season: "2020/21"
    active: true
    api: true
---
