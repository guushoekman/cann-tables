# Cann Tables

An alternative way to display the traditional league table | [canntables.com](https://canntables.com)

## Run it locally

Running Cann Tables locally is pretty easy. It uses [Jekyll](https://jekyllrb.com/) to generate the site.

This short guide assumes that you have both [Ruby](https://www.ruby-lang.org/en/downloads/) and [RubyGems](https://rubygems.org/pages/download) installed. If you do not have Ruby and RubyGems installed, do that first.

1. Clone the repository
2. Install [`bundler`](https://bundler.io/), a gem manager, using `gem install bundler`
3. Go the the project's directory: `cd cann-tables`
4. Install the project's required gems by running `bundle install`
5. Use Jekyll to generate the page `bundle exec jekyll serve`
6. The site should be available at [`http://127.0.0.1:4000/`](http://127.0.0.1:4000/)

## Let's Encrypt

This is now done automatically by GitLab, but just in case here's the command to do it manually:

`sudo letsencrypt certonly -d canntables.com --manual`