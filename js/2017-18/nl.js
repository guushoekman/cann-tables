var nl = {
  "competition": "Eredivisie",
  "code_string": "nl",
  "source": "https://en.wikipedia.org/wiki/2017%E2%80%9318_Eredivisie#League_table",
  "teams": 18,
  "cl": [1,2],
  "el": [3,4,6],
  "relegation": [16,17,18],
  "standings": [
    {
      "position": 1,
      "name": "PSV Eindhoven",
      "played": 34,
      "points": 83,
      "difference": 48
    },
    {
      "position": 2,
      "name": "Ajax",
      "played": 34,
      "points": 79,
      "difference": 56
    },
    {
      "position": 3,
      "name": "AZ",
      "played": 34,
      "points": 71,
      "difference": 34
    },
    {
      "position": 4,
      "name": "Feyenoord",
      "played": 34,
      "points": 66,
      "difference": 37
    },
    {
      "position": 5,
      "name": "Utrecht",
      "played": 34,
      "points": 54,
      "difference": 5
    },
    {
      "position": 6,
      "name": "Vitesse",
      "played": 34,
      "points": 49,
      "difference": 16
    },
    {
      "position": 7,
      "name": "ADO Den Haag",
      "played": 34,
      "points": 47,
      "difference": "−8"
    },
    {
      "position": 8,
      "name": "Heerenveen",
      "played": 34,
      "points": 46,
      "difference": "−5"
    },
    {
      "position": 9,
      "name": "PEC Zwolle",
      "played": 34,
      "points": 44,
      "difference": "−12"
    },
    {
      "position": 10,
      "name": "Heracles Almelo",
      "played": 34,
      "points": 42,
      "difference": "−14"
    },
    {
      "position": 11,
      "name": "Excelsior",
      "played": 34,
      "points": 40,
      "difference": "−15"
    },
    {
      "position": 12,
      "name": "Groningen",
      "played": 34,
      "points": 38,
      "difference": 0
    },
    {
      "position": 13,
      "name": "Willem II",
      "played": 34,
      "points": 37,
      "difference": "−13"
    },
    {
      "position": 14,
      "name": "NAC Breda",
      "played": 34,
      "points": 34,
      "difference": "−16"
    },
    {
      "position": 15,
      "name": "VVV-Venlo",
      "played": 34,
      "points": 34,
      "difference": "−19"
    },
    {
      "position": 16,
      "name": "Roda JC Kerkrade",
      "played": 34,
      "points": 30,
      "difference": "−27"
    },
    {
      "position": 17,
      "name": "Sparta Rotterdam",
      "played": 34,
      "points": 27,
      "difference": "−41"
    },
    {
      "position": 18,
      "name": "Twente",
      "played": 34,
      "points": 24,
      "difference": "−26"
    }
  ]
}