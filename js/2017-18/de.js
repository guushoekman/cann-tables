var de = {
  "competition": "Bundesliga",
  "code_string": "de",
  "source": "https://en.wikipedia.org/wiki/2017%E2%80%9318_Bundesliga#League_table",
  "teams": 18,
  "cl": [1,2,3,4],
  "el": [5,6,8],
  "relegation": [17,18],
  "standings": [
    {
      "position": 1,
      "name": "Bayern Munich",
      "played": 34,
      "points": 84,
      "difference": 64
    },
    {
      "position": 2,
      "name": "Schalke 04",
      "played": 34,
      "points": 63,
      "difference": 16
    },
    {
      "position": 3,
      "name": "1899 Hoffenheim",
      "played": 34,
      "points": 55,
      "difference": 18
    },
    {
      "position": 4,
      "name": "Borussia Dortmund",
      "played": 34,
      "points": 55,
      "difference": 17
    },
    {
      "position": 5,
      "name": "Bayer Leverkusen",
      "played": 34,
      "points": 55,
      "difference": 14
    },
    {
      "position": 6,
      "name": "RB Leipzig",
      "played": 34,
      "points": 53,
      "difference": 4
    },
    {
      "position": 7,
      "name": "VfB Stuttgart",
      "played": 34,
      "points": 51,
      "difference": 0
    },
    {
      "position": 8,
      "name": "Eintracht Frankfurt",
      "played": 34,
      "points": 49,
      "difference": 0
    },
    {
      "position": 9,
      "name": "Borussia Mönchengladbach",
      "played": 34,
      "points": 47,
      "difference": "−5"
    },
    {
      "position": 10,
      "name": "Hertha BSC",
      "played": 34,
      "points": 43,
      "difference": "−3"
    },
    {
      "position": 11,
      "name": "Werder Bremen",
      "played": 34,
      "points": 42,
      "difference": "−3"
    },
    {
      "position": 12,
      "name": "FC Augsburg",
      "played": 34,
      "points": 41,
      "difference": "−3"
    },
    {
      "position": 13,
      "name": "Hannover 96",
      "played": 34,
      "points": 39,
      "difference": "−10"
    },
    {
      "position": 14,
      "name": "Mainz 05",
      "played": 34,
      "points": 36,
      "difference": "−14"
    },
    {
      "position": 15,
      "name": "SC Freiburg",
      "played": 34,
      "points": 36,
      "difference": "−24"
    },
    {
      "position": 16,
      "name": "VfL Wolfsburg",
      "played": 34,
      "points": 33,
      "difference": "−12"
    },
    {
      "position": 17,
      "name": "Hamburger SV",
      "played": 34,
      "points": 31,
      "difference": "−24"
    },
    {
      "position": 18,
      "name": "1. FC Köln",
      "played": 34,
      "points": 22,
      "difference": "−35"
    }
  ]
}