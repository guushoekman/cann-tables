var md = {
  "competition": "Moldovan National Division",
  "code_string": "md",
  "source": "https://en.wikipedia.org/wiki/2017_Moldovan_National_Division#League_table",
  "teams": 10,
  "cl": [1],
  "el": [2,3,5],
  "relegation": [10],
  "standings": [
    {
      "position": 1,
      "name": "Sheriff Tiraspol",
      "played": 18,
      "points": 45,
      "difference": 36
    },
    {
      "position": 2,
      "name": "Milsami Orhei",
      "played": 18,
      "points": 40,
      "difference": 14
    },
    {
      "position": 3,
      "name": "Petrocub-Hîncești",
      "played": 18,
      "points": 26,
      "difference": 9
    },
    {
      "position": 4,
      "name": "Dacia Chișinău",
      "played": 18,
      "points": 26,
      "difference": -3
    },
    {
      "position": 5,
      "name": "Zaria Bălți",
      "played": 18,
      "points": 24,
      "difference": 8
    },
    {
      "position": 6,
      "name": "Speranța Nisporeni",
      "played": 18,
      "points": 21,
      "difference": -3
    },
    {
      "position": 7,
      "name": "Sfântul Gheorghe",
      "played": 18,
      "points": 20,
      "difference": -12
    },
    {
      "position": 8,
      "name": "Zimbru Chișinău",
      "played": 18,
      "points": 19,
      "difference": -4
    },
    {
      "position": 9,
      "name": "Dinamo-Auto Tiraspol",
      "played": 18,
      "points": 15,
      "difference": -24
    },
    {
      "position": 10,
      "name": "Spicul Chișcăreni",
      "played": 18,
      "points": 14,
      "difference": -21
    }
  ]
}