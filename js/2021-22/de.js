var de = {
    "competition": "Bundesliga",
    "code_string": "de",
    "source": "https://en.wikipedia.org/wiki/2021%E2%80%9322_Bundesliga#League_table",
    "teams": 18,
    "cl": [1,2,3,4],
    "el": [5,6],
    "ecl": [7],
    "relegation_play_offs": [16],
    "relegation": [17,18],
    "standings": [
     {
       "position": 1,
       "name": "Bayern Munich",
       "played": 34,
       "difference": "60",
       "points": 77
     },
     {
       "position": 2,
       "name": "Borussia Dortmund",
       "played": 34,
       "difference": "33",
       "points": 69
     },
     {
       "position": 3,
       "name": "Bayer Leverkusen",
       "played": 34,
       "difference": "33",
       "points": 64
     },
     {
       "position": 4,
       "name": "RB Leipzig",
       "played": 34,
       "difference": "35",
       "points": 58
     },
     {
       "position": 5,
       "name": "Union Berlin",
       "played": 34,
       "difference": "6",
       "points": 57
     },
     {
       "position": 6,
       "name": "SC Freiburg",
       "played": 34,
       "difference": "12",
       "points": 55
     },
     {
       "position": 7,
       "name": "1. FC Köln",
       "played": 34,
       "difference": "3",
       "points": 52
     },
     {
       "position": 8,
       "name": "Mainz 05",
       "played": 34,
       "difference": "5",
       "points": 46
     },
     {
       "position": 9,
       "name": "1899 Hoffenheim",
       "played": 34,
       "difference": "−2 ",
       "points": 46
     },
     {
       "position": 10,
       "name": "Borussia Mönchengladbach",
       "played": 34,
       "difference": "−7 ",
       "points": 45
     },
     {
       "position": 11,
       "name": "Eintracht Frankfurt",
       "played": 34,
       "difference": "−4 ",
       "points": 42
     },
     {
       "position": 12,
       "name": "VfL Wolfsburg",
       "played": 34,
       "difference": "−11 ",
       "points": 42
     },
     {
       "position": 13,
       "name": "VfL Bochum",
       "played": 34,
       "difference": "−14 ",
       "points": 42
     },
     {
       "position": 14,
       "name": "FC Augsburg",
       "played": 34,
       "difference": "−17 ",
       "points": 38
     },
     {
       "position": 15,
       "name": "VfB Stuttgart",
       "played": 34,
       "difference": "−18 ",
       "points": 33
     },
     {
       "position": 16,
       "name": "Hertha BSC",
       "played": 34,
       "difference": "−34 ",
       "points": 33
     },
     {
       "position": 17,
       "name": "Arminia Bielefeld",
       "played": 34,
       "difference": "−26 ",
       "points": 28
     },
     {
       "position": 18,
       "name": "Greuther Fürth",
       "played": 34,
       "difference": "−54 ",
       "points": 18
     }
    ]
}