var pt = {
    "competition": "Primeira Liga",
    "code_string": "pt",
    "source": "https://en.wikipedia.org/wiki/2019%E2%80%9320_Primeira_Liga#League_table",
    "teams": 18,
    "cl": [1,2,3],
    // "el": [4],
    "ecl": [4,5],
    "relegation_play_offs": [16],
    "relegation": [17,18],
    "standings": [
     {
       "position": 1,
       "name": "Porto",
       "played": 34,
       "difference": "64",
       "points": 91
     },
     {
       "position": 2,
       "name": "Sporting CP",
       "played": 34,
       "difference": "50",
       "points": 85
     },
     {
       "position": 3,
       "name": "Benfica",
       "played": 34,
       "difference": "48",
       "points": 74
     },
     {
       "position": 4,
       "name": "Braga",
       "played": 34,
       "difference": "21",
       "points": 65
     },
     {
       "position": 5,
       "name": "Gil Vicente",
       "played": 34,
       "difference": "5",
       "points": 51
     },
     {
       "position": 6,
       "name": "Vitória de Guimarães",
       "played": 34,
       "difference": "9",
       "points": 48
     },
     {
   "position": 7,
   "name": "Santa Clara",
   "played": 34,
   "difference": "−16 ",
   "points": 40
 },
 {
   "position": 8,
   "name": "Famalicão",
   "played": 34,
   "difference": "−6 ",
   "points": 39
 },
 {
   "position": 9,
   "name": "Estoril",
   "played": 34,
   "difference": "−7 ",
   "points": 39
 },
 {
   "position": 10,
   "name": "Marítimo",
   "played": 34,
   "difference": "−5 ",
   "points": 38
 },
 {
   "position": 11,
   "name": "Boavista",
   "played": 34,
   "difference": "−13 ",
   "points": 38
 },
 {
   "position": 12,
   "name": "Portimonense",
   "played": 34,
   "difference": "−14 ",
   "points": 38
 },
 {
   "position": 13,
   "name": "Paços de Ferreira",
   "played": 34,
   "difference": "−15 ",
   "points": 38
 },
 {
   "position": 14,
   "name": "Vizela",
   "played": 34,
   "difference": "−21 ",
   "points": 33
 },
 {
   "position": 15,
   "name": "Arouca",
   "played": 34,
   "difference": "−24 ",
   "points": 31
 },
 {
   "position": 16,
   "name": "Moreirense",
   "played": 34,
   "difference": "−18 ",
   "points": 29
 },
 {
   "position": 17,
   "name": "Tondela",
   "played": 34,
   "difference": "−26 ",
   "points": 28
 },
 {
   "position": 18,
   "name": "Belenenses SAD",
   "played": 34,
   "difference": "−32 ",
   "points": 26
 }
]
}