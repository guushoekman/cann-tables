var nl = {
  "competition": "Eredivisie",
  "code_string": "nl",
  "source": "https://en.wikipedia.org/wiki/2021%E2%80%9322_Eredivisie#Standings",
  "teams": 18,
  "cl": [1, 2],
  "el": [3],
  "ecl": [4],
  "ecl_play_offs": [5,6,7,8],
  "relegation_play_offs": [16],
  "relegation": [17,18],
  "standings": [
   {
     "position": 1,
     "name": "Ajax",
     "played": 34,
     "difference": "79",
     "points": 83
   },
   {
     "position": 2,
     "name": "PSV Eindhoven",
     "played": 34,
     "difference": "44",
     "points": 81
   },
   {
     "position": 3,
     "name": "Feyenoord",
     "played": 34,
     "difference": "42",
     "points": 71
   },
   {
     "position": 4,
     "name": "Twente",
     "played": 34,
     "difference": "18",
     "points": 68
   },
   {
     "position": 5,
     "name": "AZ",
     "played": 34,
     "difference": "20",
     "points": 61
   },
   {
     "position": 6,
     "name": "Vitesse",
     "played": 34,
     "difference": "−9 ",
     "points": 51
   },
   {
     "position": 7,
     "name": "Utrecht",
     "played": 34,
     "difference": "5",
     "points": 47
   },
   {
     "position": 8,
     "name": "SC Heerenveen",
     "played": 34,
     "difference": "−13 ",
     "points": 41
   },
   {
     "position": 9,
     "name": "Cambuur",
     "played": 34,
     "difference": "−17 ",
     "points": 39
   },
   {
     "position": 10,
     "name": "RKC Waalwijk",
     "played": 34,
     "difference": "−11 ",
     "points": 38
   },
   {
     "position": 11,
     "name": "NEC",
     "played": 34,
     "difference": "−14 ",
     "points": 38
   },
   {
     "position": 12,
     "name": "Groningen",
     "played": 34,
     "difference": "−14 ",
     "points": 36
   },
   {
     "position": 13,
     "name": "Go Ahead Eagles",
     "played": 34,
     "difference": "−14 ",
     "points": 36
   },
   {
     "position": 14,
     "name": "Sparta Rotterdam",
     "played": 34,
     "difference": "−18 ",
     "points": 35
   },
   {
     "position": 15,
     "name": "Fortuna Sittard",
     "played": 34,
     "difference": "−31 ",
     "points": 35
   },
   {
     "position": 16,
     "name": "Heracles Almelo",
     "played": 34,
     "difference": "−16 ",
     "points": 34
   },
   {
     "position": 17,
     "name": "Willem II",
     "played": 34,
     "difference": "−25 ",
     "points": 33
   },
   {
     "position": 18,
     "name": "PEC Zwolle",
     "played": 34,
     "difference": "−26 ",
     "points": 27
   }
  ]
}