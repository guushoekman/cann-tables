var md = {
  "competition": "Divizia Națională",
  "code_string": "md",
  "source": "https://en.wikipedia.org/wiki/2018_Moldovan_National_Division#League_table",
  "teams": 8,
  "cl": [1],
  "el": [2,3,4],
  "relegation": [8],
  "standings": [
    {
      "position": 1,
      "name": "Sheriff Tiraspol",
      "played": 28,
      "points": 63,
      "difference": 44
    },
    {
      "position": 2,
      "name": "Milsami Orhei",
      "played": 28,
      "points": 45,
      "difference": 12
    },
    {
      "position": 3,
      "name": "Petrocub-Hîncești",
      "played": 28,
      "points": 45,
      "difference": 10
    },
    {
      "position": 4,
      "name": "Speranța Nisporeni",
      "played": 28,
      "points": 38,
      "difference": 1
    },
    {
      "position": 5,
      "name": "Zimbru Chișinău",
      "played": 28,
      "points": 36,
      "difference": -9
    },
    {
      "position": 6,
      "name": "Dinamo-Auto",
      "played": 28,
      "points": 28,
      "difference": -18
    },
    {
      "position": 7,
      "name": "Sfântul Gheorghe",
      "played": 28,
      "points": 26,
      "difference": -20
    },
    {
      "position": 8,
      "name": "Zaria Bălți",
      "played": 28,
      "points": 22,
      "difference": -20
    }
  ]
}