var ru = {
  "competition": "Russian Premier League",
  "code_string": "ru",
  "source": "https://en.wikipedia.org/wiki/2018–19_Russian_Premier_League#League_table",
  "teams": 16,
  "cl": [1,2,3],
  "el": [4,5,6],
  "relegation": [15,16],
  "standings": [
  {
    "position": 1,
    "name": "Zenit Saint Petersburg",
    "played": 30,
    "points": 64,
    "difference": 28
  },
  {
    "position": 2,
    "name": "Lokomotiv Moscow",
    "played": 30,
    "points": 56,
    "difference": 17
  },
  {
    "position": 3,
    "name": "Krasnodar",
    "played": 30,
    "points": 56,
    "difference": 32
  },
  {
    "position": 4,
    "name": "CSKA Moscow",
    "played": 30,
    "points": 51,
    "difference": 23
  },
  {
    "position": 5,
    "name": "Spartak Moscow",
    "played": 30,
    "points": 49,
    "difference": 5
  },
  {
    "position": 6,
    "name": "Arsenal Tula",
    "played": 30,
    "points": 46,
    "difference": 7
  },
  {
    "position": 7,
    "name": "Orenburg",
    "played": 30,
    "points": 43,
    "difference": 5
  },
  {
    "position": 8,
    "name": "Akhmat Grozny",
    "played": 30,
    "points": 42,
    "difference": "−1"
  },
  {
    "position": 9,
    "name": "Rostov",
    "played": 30,
    "points": 41,
    "difference": 2
  },
  {
    "position": 10,
    "name": "Ural Yekaterinburg",
    "played": 30,
    "points": 38,
    "difference": "−12"
  },
  {
    "position": 11,
    "name": "Rubin Kazan",
    "played": 30,
    "points": 36,
    "difference": "−6"
  },
  {
    "position": 12,
    "name": "Dynamo Moscow",
    "played": 30,
    "points": 33,
    "difference": 0
  },
  {
    "position": 13,
    "name": "Krylia Sovetov Samara",
    "played": 30,
    "points": 28,
    "difference": "−21"
  },
  {
    "position": 14,
    "name": "Ufa",
    "played": 30,
    "points": 26,
    "difference": "−10"
  },
  {
    "position": 15,
    "name": "Anzhi Makhachkala",
    "played": 30,
    "points": 21,
    "difference": "−37"
  },
  {
    "position": 16,
    "name": "Yenisey Krasnoyarsk",
    "played": 30,
    "points": 20,
    "difference": "−31"
  }
 ]
}