// all opionts for points per game
var normalisedPoints = [30,29,28,27,26,25,24,23,22,21,20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0]

function addSeason(league, season) {
  // create useful variables
  var code_string = league["code_string"];
  var teams = league.standings;
  var number_teams = league["teams"];

  // create columns
  $("<div class='column league' data-season='" + season + "'><table class='is-fullwidth table' data-season='" + season + "'><thead><tr><th class='header-teams'><a href='/leagues/" + code_string + "/" + season + ".html'>" + season + "</a></th></tr></thead><tbody></tbody></table></div>").appendTo(".tables");

  // create rows for total points tables
  for (var i = mostPointsAllSeasons; i >= fewestPointsAllSeasons; i--) {
    $("<tr id='" + code_string + "-" + i + "'><td class='teams'><div class='circle-wrap'></div></td></tr>").appendTo(".total.columns table[data-season='" + season + "'] tbody");
  };

  // add each team for total points
  $.each(teams, function() {
    var team = $(this)[0];
    team.name = team.name.replace(/'/g, '&apos;');
    $("<span class='team-circle tooltip' data-rank='" + team.position + "' data-tooltip='" + team.position + ". " + team.name + " (" + team.points + " points, " + team.played + " games)'></span>").appendTo($(".total.columns table[data-season='" + season + "'] #" + code_string + "-" + team.points + " .teams .circle-wrap"))
  });

  // create rows for points per game
  $(normalisedPoints).each(function() {
    $("<tr id='" + code_string + "-" + this + "'><td class='teams'><div class='circle-wrap'></div></td></tr>").appendTo(".per-game.columns table[data-season='" + season + "'] tbody");
  });

  // add each team for points per game
  $.each(teams, function() {
    var team = $(this)[0];
    team.name = team.name.replace(/'/g, '&apos;');

    var pointsPerGame = team.points / team.played;
    var roundedPointsPerGame = pointsPerGame.toFixed(1) * 10;

    $("<span class='team-circle tooltip' data-rank='" + team.position + "' data-tooltip='" + team.position + ". " + team.name + " (" + pointsPerGame.toFixed(2) + " points per game, " + team.played + " games)'></span>").appendTo($(".per-game.columns table[data-season='" + season + "'] #" + code_string + "-" + roundedPointsPerGame + " .teams .circle-wrap"))
  });

  // give circles relevant colours for all tables
  $(league["cl"]).each(function(){
    var rank = Number(this);
    $("table[data-season='"+ season + "'] .team-circle[data-rank='" + rank + "']").css("background-color", champions_league);
  })

  if ("el" in league !== false)   {
    $(league["el"]).each(function(){
      var rank = Number(this);
      $("table[data-season='"+ season + "'] .team-circle[data-rank='" + rank + "']").css("background-color", europa_league);
    })
    $(".legend .el").removeClass("hidden");
  }

  if ("european_play_offs" in league !== false)   {
    $(league["european_play_offs"]).each(function(){
      var rank = Number(this);
      $("table[data-season='"+ season + "'] .team-circle[data-rank='" + rank + "']").css("background-color", european_playoffs);
    })
    $(".legend .el-playoffs").removeClass("hidden");
  }

  $(league["ecl"]).each(function(){
    var rank = Number(this);
    $("table[data-season='"+ season + "'] .team-circle[data-rank='" + rank + "']").css("background-color", conference_league);
  })

  if ("ecl_play_offs" in league !== false)   {
    $(league["ecl_play_offs"]).each(function(){
      var rank = Number(this);
      $("table[data-season='"+ season + "'] .team-circle[data-rank='" + rank + "']").css("background-color", ecl_playoffs);
    })
    $(".legend .ecl-playoffs").removeClass("hidden");
  }

  if ("relegation_play_offs" in league !== false)   {
    $(league["relegation_play_offs"]).each(function(){
      var rank = Number(this);
      $("table[data-season='"+ season + "'] .team-circle[data-rank='" + rank + "']").css("background-color", relegation_playoffs);
    })
    $(".legend .relegation-playoffs").removeClass("hidden");
  }

  $(league["relegation"]).each(function(){
    var rank = Number(this);
    $("table[data-season='"+ season + "'] .team-circle[data-rank='" + rank + "']").css("background-color", relegation);
  })
}

// show grid
function showGrid() {
  $(".columns.tables").toggleClass("is-grid");
}

// switch table tabs
$("section.league-comparison .tabs a").click(function(){
  var tab = $(this).data("tab");

  if (tab == "fullscreen") {
    $("section.league-comparison").toggleClass("fullscreen");
  } else {
    $(".columns.tables, section.league-comparison .tabs li").removeClass("is-active");
    $(this).parent().addClass("is-active");

    $(".columns.tables." + tab).addClass("is-active");
  }
});

// dropdown to show/hide seasons
$(".season.dropdown .dropdown-item").click(function() {
  var season = $(this).data("season");
  $(this).toggleClass("is-active");
  $(".column[data-season='" + season + "']").toggleClass("is-active");

  // reset tooltip position
  $(".tooltip").removeClass("is-tooltip-right is-tooltip-left");

  // tooltip on the right on first column for better visibility
  $(".columns.tables.per-game .column.is-active").first().find(".tooltip").addClass("is-tooltip-right");
  $(".columns.tables .column.is-active").first().find(".tooltip").addClass("is-tooltip-right");

  // tooltip on the left on last column for better visibility
  $(".columns.tables.per-game .column.is-active").last().find(".tooltip").addClass("is-tooltip-left");
  $(".columns.tables .column.is-active").last().find(".tooltip").addClass("is-tooltip-left");
})
