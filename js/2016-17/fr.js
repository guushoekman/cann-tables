var fr = {
  "competition": "Ligue 1",
  "code_string": "fr",
  "source": "https://en.wikipedia.org/wiki/2016%E2%80%9317_Ligue_1#League_table",
  "teams": 20,
  "cl": [1,2,3],
  "el": [4,5,6],
  "relegation": [18,19,20],
  "standings": [
    {
      "position": 1,
      "name": "Monaco",
      "played": 38,
      "difference": 76,
      "points": 95
    },
    {
      "position": 2,
      "name": "Paris Saint-Germain",
      "played": 38,
      "difference": 56,
      "points": 87
    },
    {
      "position": 3,
      "name": "Nice",
      "played": 38,
      "difference": 27,
      "points": 78
    },
    {
      "position": 4,
      "name": "Lyon",
      "played": 38,
      "difference": 29,
      "points": 67
    },
    {
      "position": 5,
      "name": "Marseille",
      "played": 38,
      "difference": 16,
      "points": 62
    },
    {
      "position": 6,
      "name": "Bordeaux",
      "played": 38,
      "difference": 10,
      "points": 59
    },
    {
      "position": 7,
      "name": "Nantes",
      "played": 38,
      "difference": "−14",
      "points": 51
    },
    {
      "position": 8,
      "name": "Saint-Étienne",
      "played": 38,
      "difference": "−1",
      "points": 50
    },
    {
      "position": 9,
      "name": "Rennes",
      "played": 38,
      "difference": "−6",
      "points": 50
    },
    {
      "position": 10,
      "name": "Guingamp",
      "played": 38,
      "difference": "−7",
      "points": 50
    },
    {
      "position": 11,
      "name": "Lille",
      "played": 38,
      "difference": "−7",
      "points": 46
    },
    {
      "position": 12,
      "name": "Angers",
      "played": 38,
      "difference": "−9",
      "points": 46
    },
    {
      "position": 13,
      "name": "Toulouse",
      "played": 38,
      "difference": "−4",
      "points": 44
    },
    {
      "position": 14,
      "name": "Metz",
      "played": 38,
      "difference": "−33",
      "points": 43
    },
    {
      "position": 15,
      "name": "Montpellier",
      "played": 38,
      "difference": "−18",
      "points": 39
    },
    {
      "position": 16,
      "name": "Dijon",
      "played": 38,
      "difference": "−12",
      "points": 37
    },
    {
      "position": 17,
      "name": "Caen",
      "played": 38,
      "difference": "−29",
      "points": 37
    },
    {
      "position": 18,
      "name": "Lorient",
      "played": 38,
      "difference": "−26",
      "points": 36
    },
    {
      "position": 19,
      "name": "Nancy",
      "played": 38,
      "difference": "−23",
      "points": 35
    },
    {
      "position": 20,
      "name": "Bastia",
      "played": 38,
      "difference": "−25",
      "points": 34
    }
  ]
}