var es = {
  "competition": "La Liga",
  "code_string": "es",
  "source": "https://en.wikipedia.org/wiki/2016%E2%80%9317_La_Liga#League_table",
  "teams": 20,
  "cl": [1,2,3,4],
  "el": [5,6,7],
  "relegation": [18,19,20],
  "standings": [
    {
      "position": 1,
      "name": "Real Madrid",
      "played": 38,
      "difference": 65,
      "points": 93
    },
    {
      "position": 2,
      "name": "Barcelona",
      "played": 38,
      "difference": 79,
      "points": 90
    },
    {
      "position": 3,
      "name": "Atlético Madrid",
      "played": 38,
      "difference": 43,
      "points": 78
    },
    {
      "position": 4,
      "name": "Sevilla",
      "played": 38,
      "difference": 20,
      "points": 72
    },
    {
      "position": 5,
      "name": "Villarreal",
      "played": 38,
      "difference": 23,
      "points": 67
    },
    {
      "position": 6,
      "name": "Real Sociedad",
      "played": 38,
      "difference": 6,
      "points": 64
    },
    {
      "position": 7,
      "name": "Athletic Bilbao",
      "played": 38,
      "difference": 10,
      "points": 63
    },
    {
      "position": 8,
      "name": "Espanyol",
      "played": 38,
      "difference": "−1",
      "points": 56
    },
    {
      "position": 9,
      "name": "Alavés",
      "played": 38,
      "difference": "−2",
      "points": 55
    },
    {
      "position": 10,
      "name": "Eibar",
      "played": 38,
      "difference": 5,
      "points": 54
    },
    {
      "position": 11,
      "name": "Málaga",
      "played": 38,
      "difference": "−6",
      "points": 46
    },
    {
      "position": 12,
      "name": "Valencia",
      "played": 38,
      "difference": "−9",
      "points": 46
    },
    {
      "position": 13,
      "name": "Celta Vigo",
      "played": 38,
      "difference": "−16",
      "points": 45
    },
    {
      "position": 14,
      "name": "Las Palmas",
      "played": 38,
      "difference": "−21",
      "points": 39
    },
    {
      "position": 15,
      "name": "Real Betis",
      "played": 38,
      "difference": "−23",
      "points": 39
    },
    {
      "position": 16,
      "name": "Deportivo La Coruña",
      "played": 38,
      "difference": "−18",
      "points": 36
    },
    {
      "position": 17,
      "name": "Leganés",
      "played": 38,
      "difference": "−19",
      "points": 35
    },
    {
      "position": 18,
      "name": "Sporting Gijón",
      "played": 38,
      "difference": "−30",
      "points": 31
    },
    {
      "position": 19,
      "name": "Osasuna",
      "played": 38,
      "difference": "−54",
      "points": 22
    },
    {
      "position": 20,
      "name": "Granada",
      "played": 38,
      "difference": "−52",
      "points": 20
    }
  ]
}