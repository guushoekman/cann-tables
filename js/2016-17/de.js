var de = {
  "competition": "Bundesliga",
  "code_string": "de",
  "source": "https://en.wikipedia.org/wiki/2016%E2%80%9317_Bundesliga#League_table",
  "teams": 18,
  "cl": [1,2,3,4],
  "el": [5,6,7],
  "relegation": [17,18],
  "standings": [
    {
      "position": 1,
      "name": "Bayern Munich",
      "played": 34,
      "points": 82,
      "difference": 67
    },
    {
      "position": 2,
      "name": "RB Leipzig",
      "played": 34,
      "points": 67,
      "difference": 27
    },
    {
      "position": 3,
      "name": "Borussia Dortmund",
      "played": 34,
      "points": 64,
      "difference": 32
    },
    {
      "position": 4,
      "name": "1899 Hoffenheim",
      "played": 34,
      "points": 62,
      "difference": 27
    },
    {
      "position": 5,
      "name": "1. FC Köln",
      "played": 34,
      "points": 49,
      "difference": 9
    },
    {
      "position": 6,
      "name": "Hertha BSC",
      "played": 34,
      "points": 49,
      "difference": "−4"
    },
    {
      "position": 7,
      "name": "SC Freiburg",
      "played": 34,
      "points": 48,
      "difference": "−18"
    },
    {
      "position": 8,
      "name": "Werder Bremen",
      "played": 34,
      "points": 45,
      "difference": "−3"
    },
    {
      "position": 9,
      "name": "Borussia Mönchengladbach",
      "played": 34,
      "points": 45,
      "difference": "−4"
    },
    {
      "position": 10,
      "name": "Schalke 04",
      "played": 34,
      "points": 43,
      "difference": 5
    },
    {
      "position": 11,
      "name": "Eintracht Frankfurt",
      "played": 34,
      "points": 42,
      "difference": "−7"
    },
    {
      "position": 12,
      "name": "Bayer Leverkusen",
      "played": 34,
      "points": 41,
      "difference": "−2"
    },
    {
      "position": 13,
      "name": "FC Augsburg",
      "played": 34,
      "points": 38,
      "difference": "−16"
    },
    {
      "position": 14,
      "name": "Hamburger SV",
      "played": 34,
      "points": 38,
      "difference": "−28"
    },
    {
      "position": 15,
      "name": "Mainz 05",
      "played": 34,
      "points": 37,
      "difference": "−11"
    },
    {
      "position": 16,
      "name": "VfL Wolfsburg",
      "played": 34,
      "points": 37,
      "difference": "−18"
    },
    {
      "position": 17,
      "name": "FC Ingolstadt",
      "played": 34,
      "points": 32,
      "difference": "−21"
    },
    {
      "position": 18,
      "name": "Darmstadt 98",
      "played": 34,
      "points": 25,
      "difference": "−35"
    }
  ]
}