var pt = {
    "competition": "Primeira Liga",
    "code_string": "pt",
    "source": "https://en.wikipedia.org/wiki/2019%E2%80%9320_Primeira_Liga#League_table",
    "teams": 18,
    "cl": [1,2],
    "el": [3,4,5],
    "relegation": [16,18],
    "standings": [
      {
       "position": 1,
       "name": "Porto",
       "played": 34,
       "difference": 52,
       "points": 82
      },
      {
       "position": 2,
       "name": "Benfica",
       "played": 34,
       "difference": 45,
       "points": 77
      },
      {
       "position": 3,
       "name": "Braga",
       "played": 34,
       "difference": 21,
       "points": 60
      },
      {
       "position": 4,
       "name": "Sporting CP",
       "played": 34,
       "difference": 15,
       "points": 60
      },
      {
       "position": 5,
       "name": "Rio Ave",
       "played": 34,
       "difference": 12,
       "points": 55
      },
      {
       "position": 6,
       "name": "Famalicão",
       "played": 34,
       "difference": 2,
       "points": 54
      },
      {
       "position": 7,
       "name": "Vitória de Guimarães",
       "played": 34,
       "difference": 15,
       "points": 50
      },
      {
       "position": 8,
       "name": "Moreirense",
       "played": 34,
       "difference": -2,
       "points": 43
      },
      {
       "position": 9,
       "name": "Santa Clara",
       "played": 34,
       "difference": -5,
       "points": 43
      },
      {
       "position": 10,
       "name": "Gil Vicente",
       "played": 34,
       "difference": -4,
       "points": 43
      },
      {
       "position": 11,
       "name": "Marítimo",
       "played": 34,
       "difference": -8,
       "points": 39
      },
      {
       "position": 12,
       "name": "Boavista",
       "played": 34,
       "difference": -11,
       "points": 39
      },
      {
       "position": 13,
       "name": "Paços de Ferreira",
       "played": 34,
       "difference": -16,
       "points": 39
      },
      {
       "position": 14,
       "name": "Tondela",
       "played": 34,
       "difference": -14,
       "points": 36
      },
      {
       "position": 15,
       "name": "Belenenses SAD",
       "played": 34,
       "difference": -27,
       "points": 35
      },
      {
       "position": 16,
       "name": "Vitória de Setúbal",
       "played": 34,
       "difference": -16,
       "points": 34
      },
      {
       "position": 17,
       "name": "Portimonense",
       "played": 34,
       "difference": -15,
       "points": 33
      },
      {
       "position": 18,
       "name": "Aves",
       "played": 34,
       "difference": -44,
       "points": 17
      }
    ]
}