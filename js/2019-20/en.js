var en = {
    "competition": "Premier League",
    "code_string": "en",
    "source": "https://en.wikipedia.org/wiki/2019%E2%80%9320_Premier_League#League_table",
    "teams": 20,
    "cl": [1,2,3,4],
    "el": [5,6,8],
    "relegation": [18,19,20],
    "standings": [
      {
       "position": 1,
       "name": "Liverpool",
       "played": 38,
       "points": 99,
       "difference": 52
      },
      {
       "position": 2,
       "name": "Manchester City",
       "played": 38,
       "points": 81,
       "difference": 67
      },
      {
       "position": 3,
       "name": "Manchester United",
       "played": 38,
       "points": 66,
       "difference": 30
      },
      {
       "position": 4,
       "name": "Chelsea",
       "played": 38,
       "points": 66,
       "difference": 15
      },
      {
       "position": 5,
       "name": "Leicester City",
       "played": 38,
       "points": 62,
       "difference": 26
      },
      {
       "position": 6,
       "name": "Tottenham Hotspur",
       "played": 38,
       "points": 59,
       "difference": 14
      },
      {
       "position": 7,
       "name": "Wolverhampton Wanderers",
       "played": 38,
       "points": 59,
       "difference": 11
      },
      {
       "position": 8,
       "name": "Arsenal",
       "played": 38,
       "points": 56,
       "difference": 8
      },
      {
       "position": 9,
       "name": "Sheffield United",
       "played": 38,
       "points": 54,
       "difference": 0
      },
      {
       "position": 10,
       "name": "Burnley",
       "played": 38,
       "points": 54,
       "difference": -7
      },
      {
       "position": 11,
       "name": "Southampton",
       "played": 38,
       "points": 52,
       "difference": -9
      },
      {
       "position": 12,
       "name": "Everton",
       "played": 38,
       "points": 49,
       "difference": -12
      },
      {
       "position": 13,
       "name": "Newcastle United",
       "played": 38,
       "points": 44,
       "difference": -20
      },
      {
       "position": 14,
       "name": "Crystal Palace",
       "played": 38,
       "points": 43,
       "difference": -19
      },
      {
       "position": 15,
       "name": "Brighton & Hove Albion",
       "played": 38,
       "points": 41,
       "difference": -15
      },
      {
       "position": 16,
       "name": "West Ham United",
       "played": 38,
       "points": 39,
       "difference": -13
      },
      {
       "position": 17,
       "name": "Aston Villa",
       "played": 38,
       "points": 35,
       "difference": -26
      },
      {
       "position": 18,
       "name": "Bournemouth",
       "played": 38,
       "points": 34,
       "difference": -25
      },
      {
       "position": 19,
       "name": "Watford",
       "played": 38,
       "points": 34,
       "difference": -28
      },
      {
       "position": 20,
       "name": "Norwich City",
       "played": 38,
       "points": 21,
       "difference": -49
      }
    ]
}