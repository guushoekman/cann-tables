var es = {
  "competition": "La Liga",
  "code_string": "es",
  "source": "https://en.wikipedia.org/wiki/2019%E2%80%9320_La_Liga#League_table",
  "teams": 20,
  "cl": [
    1,
    2,
    3,
    4
  ],
  "el": [
    5,
    6,
    7
  ],
  "relegation": [
    18,
    19,
    20
  ],
  "standings": [
	  {
	    "position": 1,
	    "name": "Real Madrid",
	    "played": 38,
	    "difference": 45,
	    "points": 87
	  },
	  {
	    "position": 2,
	    "name": "Barcelona",
	    "played": 38,
	    "difference": 48,
	    "points": 82
	  },
	  {
	    "position": 3,
	    "name": "Atlético Madrid",
	    "played": 38,
	    "difference": 24,
	    "points": 70
	  },
	  {
	    "position": 4,
	    "name": "Sevilla",
	    "played": 38,
	    "difference": 20,
	    "points": 70
	  },
	  {
	    "position": 5,
	    "name": "Villarreal",
	    "played": 38,
	    "difference": 14,
	    "points": 60
	  },
	  {
	    "position": 6,
	    "name": "Real Sociedad",
	    "played": 38,
	    "difference": 8,
	    "points": 56
	  },
	  {
	    "position": 7,
	    "name": "Granada",
	    "played": 38,
	    "difference": 7,
	    "points": 56
	  },
	  {
	    "position": 8,
	    "name": "Getafe",
	    "played": 38,
	    "difference": 6,
	    "points": 54
	  },
	  {
	    "position": 9,
	    "name": "Valencia",
	    "played": 38,
	    "difference": "−7",
	    "points": 53
	  },
	  {
	    "position": 10,
	    "name": "Osasuna",
	    "played": 38,
	    "difference": "−8",
	    "points": 52
	  },
	  {
	    "position": 11,
	    "name": "Athletic Bilbao",
	    "played": 38,
	    "difference": 3,
	    "points": 51
	  },
	  {
	    "position": 12,
	    "name": "Levante",
	    "played": 38,
	    "difference": "−6",
	    "points": 49
	  },
	  {
	    "position": 13,
	    "name": "Valladolid",
	    "played": 38,
	    "difference": "−11",
	    "points": 42
	  },
	  {
	    "position": 14,
	    "name": "Eibar",
	    "played": 38,
	    "difference": "−17",
	    "points": 42
	  },
	  {
	    "position": 15,
	    "name": "Real Betis",
	    "played": 38,
	    "difference": "−12",
	    "points": 41
	  },
	  {
	    "position": 16,
	    "name": "Alavés",
	    "played": 38,
	    "difference": "−25",
	    "points": 39
	  },
	  {
	    "position": 17,
	    "name": "Celta Vigo",
	    "played": 38,
	    "difference": "−12",
	    "points": 37
	  },
	  {
	    "position": 18,
	    "name": "Leganés",
	    "played": 38,
	    "difference": "−21",
	    "points": 36
	  },
	  {
	    "position": 19,
	    "name": "Mallorca",
	    "played": 38,
	    "difference": "−25",
	    "points": 33
	  },
	  {
	    "position": 20,
	    "name": "Espanyol",
	    "played": 38,
	    "difference": "−31",
	    "points": 25
	  }
	]
}