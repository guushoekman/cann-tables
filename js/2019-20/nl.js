var nl = {
    "competition": "Eredivisie",
    "code_string": "nl",
    "source": "https://en.wikipedia.org/wiki/2019%E2%80%9320_Eredivisie#Standings",
    "teams": 18,
    "cl": [1, 2],
    "el": [3, 4 ,5],
    "standings": [{
        "position": 1,
        "name": "AFC Ajax",
        "played": 25,
        "points": 56,
        "difference": 45
    }, {
        "position": 2,
        "name": "AZ",
        "played": 25,
        "points": 56,
        "difference": 37
    }, {
        "position": 3,
        "name": "Feyenoord Rotterdam",
        "played": 25,
        "points": 50,
        "difference": 15
    }, {
        "position": 4,
        "name": "PSV",
        "played": 26,
        "points": 49,
        "difference": 26
    }, {
        "position": 5,
        "name": "Willem II Tilburg",
        "played": 26,
        "points": 44,
        "difference": 3
    }, {
        "position": 6,
        "name": "FC Utrecht",
        "played": 25,
        "points": 41,
        "difference": 16
    }, {
        "position": 7,
        "name": "SBV Vitesse",
        "played": 26,
        "points": 41,
        "difference": 10
    }, {
        "position": 8,
        "name": "Heracles Almelo",
        "played": 26,
        "points": 36,
        "difference": 6
    }, {
        "position": 9,
        "name": "FC Groningen",
        "played": 26,
        "points": 35,
        "difference": 1
    }, {
        "position": 10,
        "name": "SC Heerenveen",
        "played": 26,
        "points": 33,
        "difference": 0
    }, {
        "position": 11,
        "name": "Sparta Rotterdam",
        "played": 26,
        "points": 33,
        "difference": -4
    }, {
        "position": 12,
        "name": "FC Emmen",
        "played": 26,
        "points": 32,
        "difference": -13
    }, {
        "position": 13,
        "name": "VVV Venlo",
        "played": 26,
        "points": 28,
        "difference": -27
    }, {
        "position": 14,
        "name": "FC Twente '65",
        "played": 26,
        "points": 27,
        "difference": -12
    }, {
        "position": 15,
        "name": "PEC Zwolle",
        "played": 26,
        "points": 26,
        "difference": -18
    }, {
        "position": 16,
        "name": "Fortuna Sittard",
        "played": 26,
        "points": 26,
        "difference": -23
    }, {
        "position": 17,
        "name": "ADO Den Haag",
        "played": 26,
        "points": 19,
        "difference": -29
    }, {
        "position": 18,
        "name": "RKC Waalwijk",
        "played": 26,
        "points": 15,
        "difference": -33
    }]
}