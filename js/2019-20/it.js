var it = {
    "competition": "Serie A",
    "code_string": "it",
    "source": "https://en.wikipedia.org/wiki/2019%E2%80%9320_Serie_A#League_table",
    "teams": 20,
    "cl": [1,2,3,4],
    "el": [5,6,7],
    "relegation": [18,19,20],
    "standings": [
      {
       "position": 1,
       "name": "Juventus",
       "played": 38,
       "points": 83,
       "difference": 33
      },
      {
       "position": 2,
       "name": "Internazionale",
       "played": 38,
       "points": 82,
       "difference": 45
      },
      {
       "position": 3,
       "name": "Atalanta",
       "played": 38,
       "points": 78,
       "difference": 50
      },
      {
       "position": 4,
       "name": "Lazio",
       "played": 38,
       "points": 78,
       "difference": 37
      },
      {
       "position": 5,
       "name": "Roma",
       "played": 38,
       "points": 70,
       "difference": 26
      },
      {
       "position": 6,
       "name": "Milan",
       "played": 38,
       "points": 66,
       "difference": 17
      },
      {
       "position": 7,
       "name": "Napoli",
       "played": 38,
       "points": 62,
       "difference": 11
      },
      {
       "position": 8,
       "name": "Sassuolo",
       "played": 38,
       "points": 51,
       "difference": 6
      },
      {
       "position": 9,
       "name": "Hellas Verona",
       "played": 38,
       "points": 49,
       "difference": -4
      },
      {
       "position": 10,
       "name": "Fiorentina",
       "played": 38,
       "points": 49,
       "difference": 3
      },
      {
       "position": 11,
       "name": "Parma",
       "played": 38,
       "points": 49,
       "difference": -1
      },
      {
       "position": 12,
       "name": "Bologna",
       "played": 38,
       "points": 47,
       "difference": -13
      },
      {
       "position": 13,
       "name": "Udinese",
       "played": 38,
       "points": 45,
       "difference": -14
      },
      {
       "position": 14,
       "name": "Cagliari",
       "played": 38,
       "points": 45,
       "difference": -4
      },
      {
       "position": 15,
       "name": "Sampdoria",
       "played": 38,
       "points": 42,
       "difference": -17
      },
      {
       "position": 16,
       "name": "Torino",
       "played": 38,
       "points": 40,
       "difference": -22
      },
      {
       "position": 17,
       "name": "Genoa",
       "played": 38,
       "points": 39,
       "difference": -26
      },
      {
       "position": 18,
       "name": "Lecce",
       "played": 38,
       "points": 35,
       "difference": -33
      },
      {
       "position": 19,
       "name": "Brescia",
       "played": 38,
       "points": 25,
       "difference": -44
      },
      {
       "position": 20,
       "name": "SPAL",
       "played": 38,
       "points": 20,
       "difference": -50
      }
    ]
}