var fr = {
    "competition": "Ligue 1",
    "code_string": "fr",
    "source": "https://en.wikipedia.org/wiki/2019-20_Ligue_1#League_table",
    "teams": 20,
    "cl": [1,2,3],
    "el": [4,5,6],
    "relegation": [19, 20],
    "standings": [{
        "position": 1,
        "name": "Paris Saint-Germain FC",
        "played": 27,
        "points": 68,
        "difference": 51
    }, {
        "position": 2,
        "name": "Olympique de Marseille",
        "played": 28,
        "points": 56,
        "difference": 12
    }, {
        "position": 3,
        "name": "Stade Rennais FC 1901",
        "played": 28,
        "points": 50,
        "difference": 14
    }, {
        "position": 4,
        "name": "Lille OSC",
        "played": 28,
        "points": 49,
        "difference": 8
    }, {
        "position": 5,
        "name": "Stade de Reims",
        "played": 28,
        "points": 41,
        "difference": 5
    }, {
        "position": 6,
        "name": "OGC Nice",
        "played": 28,
        "points": 41,
        "difference": 3
    }, {
        "position": 7,
        "name": "Olympique Lyonnais",
        "played": 28,
        "points": 40,
        "difference": 15
    }, {
        "position": 8,
        "name": "Montpellier HSC",
        "played": 28,
        "points": 40,
        "difference": 1
    }, {
        "position": 9,
        "name": "AS Monaco FC",
        "played": 28,
        "points": 40,
        "difference": 0
    }, {
        "position": 10,
        "name": "Angers SCO",
        "played": 28,
        "points": 39,
        "difference": -5
    }, {
        "position": 11,
        "name": "RC Strasbourg Alsace",
        "played": 27,
        "points": 38,
        "difference": 0
    }, {
        "position": 12,
        "name": "FC Girondins de Bordeaux",
        "played": 28,
        "points": 37,
        "difference": 6
    }, {
        "position": 13,
        "name": "FC Nantes",
        "played": 28,
        "points": 37,
        "difference": -3
    }, {
        "position": 14,
        "name": "Stade Brestois 29",
        "played": 28,
        "points": 34,
        "difference": -3
    }, {
        "position": 15,
        "name": "FC Metz",
        "played": 28,
        "points": 34,
        "difference": -8
    }, {
        "position": 16,
        "name": "Dijon Football CÃ´te d'Or",
        "played": 28,
        "points": 30,
        "difference": -10
    }, {
        "position": 17,
        "name": "AS Saint-Étienne",
        "played": 28,
        "points": 30,
        "difference": -16
    }, {
        "position": 18,
        "name": "Nîmes Olympique",
        "played": 28,
        "points": 27,
        "difference": -15
    }, {
        "position": 19,
        "name": "Amiens SC",
        "played": 28,
        "points": 23,
        "difference": -19
    }, {
        "position": 20,
        "name": "Toulouse FC",
        "played": 28,
        "points": 13,
        "difference": -36
    }]
}