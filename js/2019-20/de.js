var de = {
    "competition": "Bundesliga",
    "code_string": "de",
    "source": "https://en.wikipedia.org/wiki/2019%E2%80%9320_Bundesliga#League_table",
    "teams": 18,
    "cl": [1,2,3,4],
    "el": [5,6,7],
    "relegation": [17,18],
    "standings": [
     {
       "position": 1,
       "name": "Bayern Munich",
       "played": 34,
       "points": 82,
       "difference": 68
     },
     {
       "position": 2,
       "name": "Borussia Dortmund",
       "played": 34,
       "points": 69,
       "difference": 43
     },
     {
       "position": 3,
       "name": "RB Leipzig",
       "played": 34,
       "points": 66,
       "difference": 44
     },
     {
       "position": 4,
       "name": "Borussia Mönchengladbach",
       "played": 34,
       "points": 65,
       "difference": 26
     },
     {
       "position": 5,
       "name": "Bayer Leverkusen",
       "played": 34,
       "points": 63,
       "difference": 17
     },
     {
       "position": 6,
       "name": "1899 Hoffenheim",
       "played": 34,
       "points": 52,
       "difference": 0
     },
     {
       "position": 7,
       "name": "VfL Wolfsburg",
       "played": 34,
       "points": 49,
       "difference": 2
     },
     {
       "position": 8,
       "name": "SC Freiburg",
       "played": 34,
       "points": 48,
       "difference": 1
     },
     {
       "position": 9,
       "name": "Eintracht Frankfurt",
       "played": 34,
       "points": 45,
       "difference": -1
     },
     {
       "position": 10,
       "name": "Hertha BSC",
       "played": 34,
       "points": 41,
       "difference": -11
     },
     {
       "position": 11,
       "name": "Union Berlin",
       "played": 34,
       "points": 41,
       "difference": -17
     },
     {
       "position": 12,
       "name": "Schalke 04",
       "played": 34,
       "points": 39,
       "difference": -20
     },
     {
       "position": 13,
       "name": "Mainz 05",
       "played": 34,
       "points": 37,
       "difference": -21
     },
     {
       "position": 14,
       "name": "1. FC Köln",
       "played": 34,
       "points": 36,
       "difference": -18
     },
     {
       "position": 15,
       "name": "FC Augsburg",
       "played": 34,
       "points": 36,
       "difference": -18
     },
     {
       "position": 16,
       "name": "Werder Bremen",
       "played": 34,
       "points": 31,
       "difference": -27
     },
     {
       "position": 17,
       "name": "Fortuna Düsseldorf",
       "played": 34,
       "points": 30,
       "difference": -31
     },
     {
       "position": 18,
       "name": "SC Paderborn",
       "played": 34,
       "points": 20,
       "difference": -37
     }
    ]
}