var be = {
  "competition": "Eerste Klasse A",
  "code_string": "be",
  "source": "https://en.wikipedia.org/wiki/2019–20_Belgian_First_Division_A#League_table",
  "teams": 16,
  "cl": [1,2],
  "el": [3,4,5],
  "standings": [
    {
      "position": 1,
      "name": "Club Brugge",
      "played": 29,
      "points": 70,
      "difference": 44
    },
    {
      "position": 2,
      "name": "Gent",
      "played": 29,
      "points": 55,
      "difference": 25
    },
    {
      "position": 3,
      "name": "Charleroi",
      "played": 29,
      "points": 54,
      "difference": 26
    },
    {
      "position": 4,
      "name": "Antwerp",
      "played": 29,
      "points": 53,
      "difference": 17
    },
    {
      "position": 5,
      "name": "Standard Liège",
      "played": 29,
      "points": 49,
      "difference": 15
    },
    {
      "position": 6,
      "name": "Mechelen",
      "played": 29,
      "points": 44,
      "difference": 3
    },
    {
      "position": 7,
      "name": "Genk",
      "played": 29,
      "points": 44,
      "difference": 3
    },
    {
      "position": 8,
      "name": "Anderlecht",
      "played": 29,
      "points": 43,
      "difference": 16
    },
    {
      "position": 9,
      "name": "Zulte Waregem",
      "played": 29,
      "points": 36,
      "difference": -8
    },
    {
      "position": 10,
      "name": "Excel Mouscron",
      "played": 29,
      "points": 36,
      "difference": -2
    },
    {
      "position": 11,
      "name": "Kortrijk",
      "played": 29,
      "points": 33,
      "difference": -4
    },
    {
      "position": 12,
      "name": "Sint-Truiden",
      "played": 29,
      "points": 33,
      "difference": -17
    },
    {
      "position": 13,
      "name": "Eupen",
      "played": 29,
      "points": 30,
      "difference": -23
    },
    {
      "position": 14,
      "name": "Cercle Brugge",
      "played": 29,
      "points": 23,
      "difference": -27
    },
    {
      "position": 15,
      "name": "Oostende",
      "played": 29,
      "points": 22,
      "difference": -29
    },
    {
      "position": 16,
      "name": "Waasland-Beveren",
      "played": 29,
      "points": 20,
      "difference": -39
    }
 ]
}