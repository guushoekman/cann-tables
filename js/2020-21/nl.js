var nl = {
    "competition": "Eredivisie",
    "code_string": "nl",
    "source": "https://en.wikipedia.org/wiki/2020%E2%80%9321_Eredivisie#Standings",
    "teams": 18,
    "cl": [1, 2],
    "el": [3],
    "ecl": [4 ,5],
    "relegation": [16, 17,18],
    "standings": [
      {
        "position": 1,
        "name": "Ajax",
        "played": 34,
        "points": 88,
        "difference": 79
      },
      {
        "position": 2,
        "name": "PSV Eindhoven",
        "played": 34,
        "points": 72,
        "difference": 39
      },
      {
        "position": 3,
        "name": "AZ",
        "played": 34,
        "points": 71,
        "difference": 34
      },
      {
        "position": 4,
        "name": "Vitesse",
        "played": 34,
        "points": 61,
        "difference": 14
      },
      {
        "position": 5,
        "name": "Feyenoord",
        "played": 34,
        "points": 59,
        "difference": 28
      },
      {
        "position": 6,
        "name": "Utrecht",
        "played": 34,
        "points": 53,
        "difference": 11
      },
      {
        "position": 7,
        "name": "Groningen",
        "played": 34,
        "points": 50,
        "difference": 3
      },
      {
        "position": 8,
        "name": "Sparta Rotterdam",
        "played": 34,
        "points": 47,
        "difference": 1
      },
      {
        "position": 9,
        "name": "Heracles Almelo",
        "played": 34,
        "points": 44,
        "difference": "−11"
      },
      {
        "position": 10,
        "name": "Twente",
        "played": 34,
        "points": 41,
        "difference": "−2"
      },
      {
        "position": 11,
        "name": "Fortuna Sittard",
        "played": 34,
        "points": 41,
        "difference": "−8"
      },
      {
        "position": 12,
        "name": "Heerenveen",
        "played": 34,
        "points": 39,
        "difference": "−6"
      },
      {
        "position": 13,
        "name": "PEC Zwolle",
        "played": 34,
        "points": 38,
        "difference": "−9"
      },
      {
        "position": 14,
        "name": "Willem II",
        "played": 34,
        "points": 31,
        "difference": "−28"
      },
      {
        "position": 15,
        "name": "RKC Waalwijk",
        "played": 34,
        "points": 30,
        "difference": "−22"
      },
      {
        "position": 16,
        "name": "Emmen",
        "played": 34,
        "points": 30,
        "difference": "−28"
      },
      {
        "position": 17,
        "name": "VVV-Venlo",
        "played": 34,
        "points": 23,
        "difference": "−48"
      },
      {
        "position": 18,
        "name": "ADO Den Haag",
        "played": 34,
        "points": 22,
        "difference": "−47"
      }
    ]
}