var pt = {
    "competition": "Primeira Liga",
    "code_string": "pt",
    "source": "https://en.wikipedia.org/wiki/2019%E2%80%9320_Primeira_Liga#League_table",
    "teams": 18,
    "cl": [1,2,3],
    "el": [4],
    "ecl": [5,6],
    "relegation": [16,17,18],
    "standings": [
      {
        "position": 1,
        "name": "Sporting CP",
        "played": 34,
        "points": 85,
        "difference": 45
      },
      {
        "position": 2,
        "name": "Porto",
        "played": 34,
        "points": 80,
        "difference": 45
      },
      {
        "position": 3,
        "name": "Benfica",
        "played": 34,
        "points": 76,
        "difference": 42
      },
      {
        "position": 4,
        "name": "Braga",
        "played": 34,
        "points": 64,
        "difference": 20
      },
      {
        "position": 5,
        "name": "Paços de Ferreira",
        "played": 34,
        "points": 53,
        "difference": "−1"
      },
      {
        "position": 6,
        "name": "Santa Clara",
        "played": 34,
        "points": 46,
        "difference": 8
      },
      {
        "position": 7,
        "name": "Vitória de Guimarães",
        "played": 34,
        "points": 43,
        "difference": "−7"
      },
      {
        "position": 8,
        "name": "Moreirense",
        "played": 34,
        "points": 43,
        "difference": "−6"
      },
      {
        "position": 9,
        "name": "Famalicão",
        "played": 34,
        "points": 40,
        "difference": "−8"
      },
      {
        "position": 10,
        "name": "Belenenses SAD",
        "played": 34,
        "points": 40,
        "difference": "−10"
      },
      {
        "position": 11,
        "name": "Gil Vicente",
        "played": 34,
        "points": 39,
        "difference": "−9"
      },
      {
        "position": 12,
        "name": "Tondela",
        "played": 34,
        "points": 36,
        "difference": "−21"
      },
      {
        "position": 13,
        "name": "Boavista",
        "played": 34,
        "points": 36,
        "difference": "−10"
      },
      {
        "position": 14,
        "name": "Portimonense",
        "played": 34,
        "points": 35,
        "difference": "−7"
      },
      {
        "position": 15,
        "name": "Marítimo",
        "played": 34,
        "points": 35,
        "difference": "−20"
      },
      {
        "position": 16,
        "name": "Rio Ave",
        "played": 34,
        "points": 34,
        "difference": "−15"
      },
      {
        "position": 17,
        "name": "Farense",
        "played": 34,
        "points": 31,
        "difference": "−17"
      },
      {
        "position": 18,
        "name": "Nacional",
        "played": 34,
        "points": 25,
        "difference": "−29"
      }
    ]
}