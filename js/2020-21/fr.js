var fr = {
    "competition": "Ligue 1",
    "code_string": "fr",
    "source": "https://en.wikipedia.org/wiki/2019-20_Ligue_1#League_table",
    "teams": 20,
    "cl": [1,2,3],
    "el": [4,5],
    "ecl": [6],
    "relegation": [19, 20],
    "standings": [
      {
        "position": 1,
        "name": "Lille",
        "played": 38,
        "points": 83,
        "difference": 41
      },
      {
        "position": 2,
        "name": "Paris Saint-Germain",
        "played": 38,
        "points": 82,
        "difference": 58
      },
      {
        "position": 3,
        "name": "Monaco",
        "played": 38,
        "points": 78,
        "difference": 34
      },
      {
        "position": 4,
        "name": "Lyon",
        "played": 38,
        "points": 76,
        "difference": 38
      },
      {
        "position": 5,
        "name": "Marseille",
        "played": 38,
        "points": 60,
        "difference": 7
      },
      {
        "position": 6,
        "name": "Rennes",
        "played": 38,
        "points": 58,
        "difference": 12
      },
      {
        "position": 7,
        "name": "Lens",
        "played": 38,
        "points": 57,
        "difference": 1
      },
      {
        "position": 8,
        "name": "Montpellier",
        "played": 38,
        "points": 54,
        "difference": "−2"
      },
      {
        "position": 9,
        "name": "Nice",
        "played": 38,
        "points": 52,
        "difference": "−3"
      },
      {
        "position": 10,
        "name": "Metz",
        "played": 38,
        "points": 47,
        "difference": "−4"
      },
      {
        "position": 11,
        "name": "Saint-Étienne",
        "played": 38,
        "points": 46,
        "difference": "−12"
      },
      {
        "position": 12,
        "name": "Bordeaux",
        "played": 38,
        "points": 45,
        "difference": "−14"
      },
      {
        "position": 13,
        "name": "Angers",
        "played": 38,
        "points": 44,
        "difference": "−18"
      },
      {
        "position": 14,
        "name": "Reims",
        "played": 38,
        "points": 42,
        "difference": "−8"
      },
      {
        "position": 15,
        "name": "Strasbourg",
        "played": 38,
        "points": 42,
        "difference": "−9"
      },
      {
        "position": 16,
        "name": "Lorient",
        "played": 38,
        "points": 42,
        "difference": "−18"
      },
      {
        "position": 17,
        "name": "Brest",
        "played": 38,
        "points": 41,
        "difference": "−16"
      },
      {
        "position": 18,
        "name": "Nantes",
        "played": 38,
        "points": 40,
        "difference": "−8"
      },
      {
        "position": 19,
        "name": "Nîmes",
        "played": 38,
        "points": 35,
        "difference": "−31"
      },
      {
        "position": 20,
        "name": "Dijon",
        "played": 38,
        "points": 21,
        "difference": "−48"
      }
    ]
}