var it = {
  "competition": "Serie A",
  "code_string": "it",
  "source": "https://en.wikipedia.org/wiki/2018%E2%80%9319_Serie_A#League_table",
  "last_updated": "2021-04-17T07:19:44Z",
  "teams": 20,
  "cl": [1,2,3,4],
  "el": [5,6],
  "ecl": [7],
  "relegation": [18,19,20],
  "standings": [
    {
      "position": 1,
      "name": "FC Internazionale Milano",
      "played": 38,
      "points": 91,
      "difference": 54
    },
    {
      "position": 2,
      "name": "AC Milan",
      "played": 38,
      "points": 79,
      "difference": 33
    },
    {
      "position": 3,
      "name": "Atalanta BC",
      "played": 38,
      "points": 78,
      "difference": 43
    },
    {
      "position": 4,
      "name": "Juventus FC",
      "played": 38,
      "points": 78,
      "difference": 39
    },
    {
      "position": 5,
      "name": "SSC Napoli",
      "played": 38,
      "points": 77,
      "difference": 45
    },
    {
      "position": 6,
      "name": "SS Lazio",
      "played": 38,
      "points": 68,
      "difference": 6
    },
    {
      "position": 7,
      "name": "AS Roma",
      "played": 38,
      "points": 62,
      "difference": 10
    },
    {
      "position": 8,
      "name": "US Sassuolo Calcio",
      "played": 38,
      "points": 62,
      "difference": 8
    },
    {
      "position": 9,
      "name": "UC Sampdoria",
      "played": 38,
      "points": 52,
      "difference": -2
    },
    {
      "position": 10,
      "name": "Hellas Verona FC",
      "played": 38,
      "points": 45,
      "difference": -2
    },
    {
      "position": 11,
      "name": "Genoa CFC",
      "played": 38,
      "points": 42,
      "difference": -11
    },
    {
      "position": 12,
      "name": "Bologna FC 1909",
      "played": 38,
      "points": 41,
      "difference": -14
    },
    {
      "position": 13,
      "name": "ACF Fiorentina",
      "played": 38,
      "points": 40,
      "difference": -12
    },
    {
      "position": 14,
      "name": "Udinese Calcio",
      "played": 38,
      "points": 40,
      "difference": -16
    },
    {
      "position": 15,
      "name": "Spezia Calcio",
      "played": 38,
      "points": 39,
      "difference": -20
    },
    {
      "position": 16,
      "name": "Cagliari Calcio",
      "played": 38,
      "points": 37,
      "difference": -16
    },
    {
      "position": 17,
      "name": "Torino FC",
      "played": 38,
      "points": 37,
      "difference": -19
    },
    {
      "position": 18,
      "name": "Benevento Calcio",
      "played": 38,
      "points": 33,
      "difference": -35
    },
    {
      "position": 19,
      "name": "FC Crotone",
      "played": 38,
      "points": 23,
      "difference": -47
    },
    {
      "position": 20,
      "name": "Parma Calcio 1913",
      "played": 38,
      "points": 20,
      "difference": -44
    }
  ]
}
