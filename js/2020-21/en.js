var en = {
    "competition": "Premier League",
    "code_string": "en",
    "source": "https://en.wikipedia.org/wiki/2020%E2%80%9321_Premier_League#League_table",
    "teams": 20,
    "cl": [1,2,3,4],
    "el": [5,6],
    "ecl": [7],
    "relegation": [18,19,20],
    "standings": [
      {
        "position": 1,
        "name": "Manchester City",
        "played": 38,
        "points": "",
        "difference": 51
      },
      {
        "position": 2,
        "name": "Manchester United",
        "played": 38,
        "points": 74,
        "difference": 29
      },
      {
        "position": 3,
        "name": "Liverpool",
        "played": 38,
        "points": 69,
        "difference": 26
      },
      {
        "position": 4,
        "name": "Chelsea",
        "played": 38,
        "points": 67,
        "difference": 22
      },
      {
        "position": 5,
        "name": "Leicester City",
        "played": 38,
        "points": 66,
        "difference": 18
      },
      {
        "position": 6,
        "name": "West Ham United",
        "played": 38,
        "points": 65,
        "difference": 15
      },
      {
        "position": 7,
        "name": "Tottenham Hotspur",
        "played": 38,
        "points": 62,
        "difference": 23
      },
      {
        "position": 8,
        "name": "Arsenal",
        "played": 38,
        "points": 61,
        "difference": 16
      },
      {
        "position": 9,
        "name": "Leeds United",
        "played": 38,
        "points": 59,
        "difference": 8
      },
      {
        "position": 10,
        "name": "Everton",
        "played": 38,
        "points": 59,
        "difference": "−1"
      },
      {
        "position": 11,
        "name": "Aston Villa",
        "played": 38,
        "points": 55,
        "difference": 9
      },
      {
        "position": 12,
        "name": "Newcastle United",
        "played": 38,
        "points": 45,
        "difference": "−16"
      },
      {
        "position": 13,
        "name": "Wolverhampton Wanderers",
        "played": 38,
        "points": 45,
        "difference": "−16"
      },
      {
        "position": 14,
        "name": "Crystal Palace",
        "played": 38,
        "points": 44,
        "difference": "−25"
      },
      {
        "position": 15,
        "name": "Southampton",
        "played": 38,
        "points": 43,
        "difference": "−21"
      },
      {
        "position": 16,
        "name": "Brighton & Hove Albion",
        "played": 38,
        "points": 41,
        "difference": "−6"
      },
      {
        "position": 17,
        "name": "Burnley",
        "played": 38,
        "points": 39,
        "difference": "−22"
      },
      {
        "position": 18,
        "name": "Fulham",
        "played": 38,
        "points": 28,
        "difference": "−26"
      },
      {
        "position": 19,
        "name": "West Bromwich Albion",
        "played": 38,
        "points": 26,
        "difference": "−41"
      },
      {
        "position": 20,
        "name": "Sheffield United",
        "played": 38,
        "points": 23,
        "difference": "−43"
      }
    ]
}