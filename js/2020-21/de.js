var de = {
    "competition": "Bundesliga",
    "code_string": "de",
    "source": "https://en.wikipedia.org/wiki/2020%E2%80%9321_Bundesliga#League_table",
    "teams": 18,
    "cl": [1,2,3,4],
    "el": [5,6],
    "ecl": [7],
    "relegation": [17,18],
    "standings": [
      {
        "position": 1,
        "name": "Bayern Munich",
        "played": 34,
        "points": 78,
        "difference": 55
      },
      {
        "position": 2,
        "name": "RB Leipzig",
        "played": 34,
        "points": 65,
        "difference": 28
      },
      {
        "position": 3,
        "name": "Borussia Dortmund",
        "played": 34,
        "points": 64,
        "difference": 29
      },
      {
        "position": 4,
        "name": "VfL Wolfsburg",
        "played": 34,
        "points": 61,
        "difference": 24
      },
      {
        "position": 5,
        "name": "Eintracht Frankfurt",
        "played": 34,
        "points": 60,
        "difference": 16
      },
      {
        "position": 6,
        "name": "Bayer Leverkusen",
        "played": 34,
        "points": 52,
        "difference": 14
      },
      {
        "position": 7,
        "name": "Union Berlin",
        "played": 34,
        "points": 50,
        "difference": 7
      },
      {
        "position": 8,
        "name": "Borussia Mönchengladbach",
        "played": 34,
        "points": 49,
        "difference": 8
      },
      {
        "position": 9,
        "name": "VfB Stuttgart",
        "played": 34,
        "points": 45,
        "difference": 1
      },
      {
        "position": 10,
        "name": "SC Freiburg",
        "played": 34,
        "points": 45,
        "difference": 0
      },
      {
        "position": 11,
        "name": "1899 Hoffenheim",
        "played": 34,
        "points": 43,
        "difference": "−2"
      },
      {
        "position": 12,
        "name": "Mainz 05",
        "played": 34,
        "points": 39,
        "difference": "−17"
      },
      {
        "position": 13,
        "name": "FC Augsburg",
        "played": 34,
        "points": 36,
        "difference": "−18"
      },
      {
        "position": 14,
        "name": "Hertha BSC",
        "played": 34,
        "points": 35,
        "difference": "−11"
      },
      {
        "position": 15,
        "name": "Arminia Bielefeld",
        "played": 34,
        "points": 35,
        "difference": "−26"
      },
      {
        "position": 16,
        "name": "1. FC Köln",
        "played": 34,
        "points": 33,
        "difference": "−26"
      },
      {
        "position": 17,
        "name": "Werder Bremen",
        "played": 34,
        "points": 31,
        "difference": "−21"
      },
      {
        "position": 18,
        "name": "Schalke 04",
        "played": 34,
        "points": 16,
        "difference": "−61"
      }
    ]
}