var es = {
  "competition": "La Liga",
  "code_string": "es",
  "source": "https://en.wikipedia.org/wiki/2020%E2%80%9321_La_Liga#League_table",
  "teams": 20,
  "cl": [1,2,3,4,7],
  "el": [5,6],
  "relegation": [18,19,20],
  "standings": [
	  {
	    "position": 1,
	    "name": "Atlético Madrid",
	    "played": 38,
	    "points": 86,
	    "difference": 42
	  },
	  {
	    "position": 2,
	    "name": "Real Madrid",
	    "played": 38,
	    "points": 84,
	    "difference": 39
	  },
	  {
	    "position": 3,
	    "name": "Barcelona",
	    "played": 38,
	    "points": 79,
	    "difference": 47
	  },
	  {
	    "position": 4,
	    "name": "Sevilla",
	    "played": 38,
	    "points": 77,
	    "difference": 20
	  },
	  {
	    "position": 5,
	    "name": "Real Sociedad",
	    "played": 38,
	    "points": 62,
	    "difference": 21
	  },
	  {
	    "position": 6,
	    "name": "Real Betis",
	    "played": 38,
	    "points": 61,
	    "difference": 0
	  },
	  {
	    "position": 7,
	    "name": "Villarreal",
	    "played": 38,
	    "points": 58,
	    "difference": 16
	  },
	  {
	    "position": 8,
	    "name": "Celta Vigo",
	    "played": 38,
	    "points": 53,
	    "difference": "−2"
	  },
	  {
	    "position": 9,
	    "name": "Granada",
	    "played": 38,
	    "points": 46,
	    "difference": "−18"
	  },
	  {
	    "position": 10,
	    "name": "Athletic Bilbao",
	    "played": 38,
	    "points": 46,
	    "difference": 4
	  },
	  {
	    "position": 11,
	    "name": "Osasuna",
	    "played": 38,
	    "points": 44,
	    "difference": "−11"
	  },
	  {
	    "position": 12,
	    "name": "Cádiz",
	    "played": 38,
	    "points": 44,
	    "difference": "−22"
	  },
	  {
	    "position": 13,
	    "name": "Valencia",
	    "played": 38,
	    "points": 43,
	    "difference": "−3"
	  },
	  {
	    "position": 14,
	    "name": "Levante",
	    "played": 38,
	    "points": 41,
	    "difference": "−11"
	  },
	  {
	    "position": 15,
	    "name": "Getafe",
	    "played": 38,
	    "points": 38,
	    "difference": "−15"
	  },
	  {
	    "position": 16,
	    "name": "Alavés",
	    "played": 38,
	    "points": 38,
	    "difference": "−21"
	  },
	  {
	    "position": 17,
	    "name": "Elche",
	    "played": 38,
	    "points": 36,
	    "difference": "−21"
	  },
	  {
	    "position": 18,
	    "name": "Huesca",
	    "played": 38,
	    "points": 34,
	    "difference": "−19"
	  },
	  {
	    "position": 19,
	    "name": "Valladolid",
	    "played": 38,
	    "points": 31,
	    "difference": "−23"
	  },
	  {
	    "position": 20,
	    "name": "Eibar",
	    "played": 38,
	    "points": 30,
	    "difference": "−23"
	  }
	]
}