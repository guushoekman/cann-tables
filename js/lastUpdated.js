var lastUpdated = new Date(2015, 11, 20);

$(leagues).each(function() {
	var code = this["code"];
  var teams = code.standings[0].table;
	var number_teams = this["teams"];

  $.each(teams, function() {
		var lastUpdatedLeague = new Date(this.code.competition.lastUpdated);
		if (lastUpdatedLeague > lastUpdated) {
			lastUpdated = lastUpdatedLeague
		};
	}
});

// show last updated
var lastUpdatedString = lastUpdated.toLocaleString('en-GB', {timeZone: 'UTC', timeZoneName: 'short', day: '2-digit', month: 'short', hour: '2-digit', minute: '2-digit'});
$(".last-update").text(lastUpdatedString)